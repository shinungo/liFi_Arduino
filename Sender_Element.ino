#include <Keypad.h>

int laser = 13;      // laser on Pin 13
String asimpleObject = ""; 
const byte ROWS = 4;
const byte COLS = 4; 
char keys[ROWS][COLS] = {
  {'1','2','3','A'},
  {'4','5','6','B'},
  {'7','8','9','C'},
  {'*','0','#','D'}
};

byte rowPins[ROWS] = {5, 4, 3, 2}; //connect to the row pinouts of the keypad
byte colPins[COLS] = {9, 8, 7, 6}; //connect to the column pinouts of the keypad

Keypad keypad = Keypad( makeKeymap(keys), rowPins, colPins, ROWS, COLS );

int dotLen = 2;     // length of the morse code 'dot'
int dashLen = dotLen * 2;    // length of the morse code 'dash'

void setup() {   
  Serial.begin(9600); // for Pad & Serial to Beginn             
  pinMode(laser, OUTPUT);    // initialize the digital pin as an output for Laser 
     }

void LightsOff(int delayTime)  {
  digitalWrite(laser, LOW);
  delay(delayTime);            
}

void LightsOn()  {
  digitalWrite(laser, HIGH);           
}
void MorseDot()  {
  digitalWrite(laser, HIGH);
  delay(dotLen);             
}
void MorseDash()  {
  digitalWrite(laser, HIGH);
  delay(dashLen);               
}

// Create a loop of the letters/words you want to output in morse code (defined in string at top of code)
void loop() { 
   LightsOn(); 
    char key = keypad.getKey();
    if (key){
     Serial.println(key);
     asimpleObject = key; 
   if (asimpleObject == ""){
    LightsOn(); 
      }
      if (asimpleObject == "1"){
    morseone(); 
      } if (asimpleObject == "2"){
    morsetwo (); 
      } if (asimpleObject == "3"){
    morsethree (); 
      } if (asimpleObject == "4"){
    morsefour (); 
      } if (asimpleObject == "5"){
    morsefive (); 
      } if (asimpleObject == "6"){
    morsesix (); 
      } if (asimpleObject == "7"){
    morseseven (); 
      } if (asimpleObject == "8"){
    morseeight (); 
      } if (asimpleObject == "9"){
    morsenine (); 
      } if (asimpleObject == "0"){
    morsenull (); 
      } if (asimpleObject == "A"){
    morsea (); 
      } if (asimpleObject == "B"){
    morseb (); 
      } if (asimpleObject == "C"){
    morsec (); 
      } if (asimpleObject == "D"){
    morsed (); 
      } if (asimpleObject == "*"){
    morsee (); 
      } if (asimpleObject == "#"){
    morsef (); 
      } 
  }
      LightsOn(); 
}

void morsenull() {
 Serial.println("NULL");
 LightsOff(dotLen); MorseDash();  LightsOff(dotLen); MorseDash();  LightsOff(dotLen); MorseDash();  LightsOff(dotLen); MorseDash();   
} 

void morseone() {
 Serial.println("EINS");
    LightsOff(dotLen); MorseDash();  LightsOff(dotLen); MorseDash();  LightsOff(dotLen); MorseDash();  LightsOff(dashLen); MorseDot();   
}
void morsetwo() {
 Serial.println("Zwei");
    LightsOff(dotLen); MorseDash();  LightsOff(dotLen); MorseDash();  LightsOff(dashLen); MorseDot();  LightsOff(dashLen); MorseDot();   
} 
void morsethree() {
 Serial.println("Drei");
    LightsOff(dotLen); MorseDash();  LightsOff(dashLen); MorseDot();  LightsOff(dashLen); MorseDot();  LightsOff(dashLen); MorseDot();   
} 
void morsefour() {
 Serial.println("VIER");
     LightsOff(dashLen); MorseDot();  LightsOff(dashLen); MorseDot();  LightsOff(dashLen); MorseDot();  LightsOff(dashLen); MorseDot();   
} 
void morsefive() {
 Serial.println("FUENF");
      LightsOff(dashLen); MorseDot();  LightsOff(dotLen); MorseDash();  LightsOff(dotLen); MorseDash();  LightsOff(dotLen); MorseDash();   
} 
void morsesix() {
 Serial.println("SECHS");
    LightsOff(dashLen); MorseDot();  LightsOff(dashLen); MorseDot();  LightsOff(dashLen); MorseDot();  LightsOff(dotLen); MorseDot();   
} 
void morseseven() {
 Serial.println("SIEBEN");
    LightsOff(dashLen); MorseDot();  LightsOff(dashLen); MorseDot();  LightsOff(dotLen); MorseDash();  LightsOff(dashLen); MorseDot();   
} 
void morseeight() {
 Serial.println("ACHT");
    LightsOff(dashLen); MorseDot();  LightsOff(dotLen); MorseDash();  LightsOff(dashLen); MorseDot();  LightsOff(dashLen); MorseDot();
}
void morsenine() {
 Serial.println("NEUN");
    LightsOff(dotLen); MorseDash();  LightsOff(dashLen); MorseDot();  LightsOff(dotLen); MorseDash();  LightsOff(dashLen); MorseDot();   
} 
void morsea() {
 Serial.println("AAAA");
       LightsOff(dotLen); MorseDash();  LightsOff(dotLen); MorseDash();  LightsOff(dashLen); MorseDot();  LightsOff(dotLen); MorseDash();   
} 
void morseb() {
 Serial.println("BBBB");
    LightsOff(dashLen); MorseDot();  LightsOff(dashLen); MorseDot();  LightsOff(dotLen); MorseDash();  LightsOff(dotLen); MorseDash();   
} 
void morsec() {
 Serial.println("CCCC");
      LightsOff(dashLen); MorseDot();  LightsOff(dotLen); MorseDash();  LightsOff(dotLen); MorseDash();  LightsOff(dashLen); MorseDot();   
} 
void morsed() {
 Serial.println("DDDD");
      LightsOff(dashLen); MorseDot();  LightsOff(dotLen); MorseDash();  LightsOff(dashLen); MorseDot();  LightsOff(dotLen); MorseDash();   
} 
void morsee() {
 Serial.println("STERN");
    LightsOff(dotLen); MorseDash();  LightsOff(dashLen); MorseDot();  LightsOff(dotLen); MorseDash();  LightsOff(dotLen); MorseDash();   
} 
void morsef() {
 Serial.println("HASCH");
   LightsOff(dotLen); MorseDash();  LightsOff(dashLen); MorseDot();  LightsOff(dashLen); MorseDot();  LightsOff(dotLen); MorseDash();   
}
